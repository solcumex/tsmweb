<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class trigrama extends Model
{

    protected $fillable = [
        'clave', 'valor', 'descripcion','cantidad',
    ];

    public $timestamps = false;

    protected $table = "trigrama";
}
