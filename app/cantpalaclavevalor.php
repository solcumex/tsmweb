<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cantpalaclavevalor extends Model
{
    protected $fillable = [
        'clave', 'valor', 'descripcion',
    ];

    public $timestamps = false;

    protected $table = "cantpalaclavevalor";
}
