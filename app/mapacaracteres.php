<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mapacaracteres extends Model
{
    protected $fillable = [
        'clave', 'valor', 'descripcion',
    ];

    public $timestamps = false;

    protected $table = "mapacaracteres";
}
