<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class oracionnumeracion extends Model
{
    protected $fillable = [
        'oracion', 'numeracion', 'descripcion',
    ];

    public $timestamps = false;

    protected $table = "oracionnumeracion";
}
