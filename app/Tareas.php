<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tareas extends Model
{
    protected $fillable = [
        'titulo', 'descripcion', 'nivel_de_prioridad','operacion',
    ];

    protected $primaryKey = 'id_tarea';
    public $timestamps = false;
}
