<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class bigrama extends Model
{
    protected $fillable = [
        'clave', 'valor', 'descripcion','cantidad',
    ];

    public $timestamps = false;
}
