<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tareaba extends Model
{
    protected $fillable = [
        'titulo', 'descripcion', 'nivel_de_prioridad','operacion','basura'
    ];

    protected $primaryKey = 'id_tarea';
    public $timestamps = false;
    public  $table='tareaba';
}
