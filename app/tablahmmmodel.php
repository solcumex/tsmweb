<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tablahmmmodel extends Model
{ protected $fillable = [
    'clave', 'clavecon3', 'clavecon4','valor','descripcion','numeracion','numeracion3','numeracion4'
];

    public $timestamps = false;

    protected $table = "tablahmm";
}
