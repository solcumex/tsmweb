<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/','TareasController@index')->name('inicio');
Route::resource('tarea','TareasController');
//Route::get('tarea','TareasController');
Route::post('tarea/{tarea}','TareasController@update')->name('tareaUpdate');
Route::get('tarea/bigrama/local','TareasController@bigrama')->name('tarea.bigrama');

Route::get('tarea/trigrama/local','TareasController@trigrama')->name('tarea.trigrama');
Route::get('tarea/markov/vitorama','TareasController@markov')->name('tarea.markov');
Route::get('tarea/markov/mapacaracteres','TareasController@mapacaracteres')->name('tarea.mapacaracteres');
Route::get('tarea/markov/oracionnumeracion','TareasController@oracionnumeracion')->name('tarea.oracionnumeracion');





Route::get('tarea/operacion/suma','TareasController@operacionessuma')->name('operaciones.suma');
Route::get('tarea/operacion/resta','TareasController@operacionesresta')->name('operaciones.resta');
Route::get('tarea/operacion/multplicacion','TareasController@operacionesmultip')->name('operaciones.multplicacion');
Route::get('tarea/operacion/division','TareasController@operacionesdivisi')->name('operaciones.division');
Route::get('tarea/operacion/variable','TareasController@operacionesvariable')->name('operaciones.variable');
Route::get('tarea/operacion/basura','TareasController@operacionesbasura')->name('operaciones.basura');
Route::get('tarea/basura/index','TareasController@Tareasbasura')->name('basura');


Route::get('tarea/hmm/index','TareasController@devolvertablaHMM')->name('devolvertablahmm');
Route::get('tarea/hmm/suma','TareasController@devolvertablaHMMsuma')->name('devolvertablahmmsuma');
Route::get('tarea/hmm/resta','TareasController@devolvertablaHMMresta')->name('devolvertablahmmresta');
Route::get('tarea/hmm/multipli','TareasController@devolvertablaHMMmultipli')->name('devolvertablahmmmultipli');
Route::get('tarea/hmm/divisi','TareasController@devolvertablaHMMdivisi')->name('devolvertablahmmdivisi');
Route::get('tarea/hmm/basura','TareasController@devolvertablaHMMbasur')->name('devolvertablahmmbasura');
Route::get('tarea/hmm/vector','TareasController@devolvertablaHMMvector')->name('devolvertablahmmvector');
