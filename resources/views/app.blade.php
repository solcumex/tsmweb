<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Laravel</title>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <style>


            .panel-table .panel-body{
                padding:0;
            }

            .panel-table .panel-body .table-bordered{
                border-style: none;
                margin:0;
            }

            .panel-table .panel-body .table-bordered > thead > tr > th:first-of-type {
                text-align:center;
                width: 100px;
            }

            .panel-table .panel-body .table-bordered > thead > tr > th:last-of-type,
            .panel-table .panel-body .table-bordered > tbody > tr > td:last-of-type {
                border-right: 0px;
            }

            .panel-table .panel-body .table-bordered > thead > tr > th:first-of-type,
            .panel-table .panel-body .table-bordered > tbody > tr > td:first-of-type {
                border-left: 0px;
            }

            .panel-table .panel-body .table-bordered > tbody > tr:first-of-type > td{
                border-bottom: 0px;
            }

            .panel-table .panel-body .table-bordered > thead > tr:first-of-type > th{
                border-top: 0px;
            }

            .panel-table .panel-footer .pagination{
                margin:0;
            }

            /*
            used to vertically center elements, may need modification if you're not using default sizes.
            */
            .panel-table .panel-footer .col{
                line-height: 34px;
                height: 34px;
            }

            .panel-table .panel-heading .col h3{
                line-height: 30px;
                height: 30px;
            }

            .panel-table .panel-body .table-bordered > tbody > tr > td{
                line-height: 34px;
            }

            .navbar-brand-centered {
                position: absolute;
                left: 50%;
                display: block;
                width: 160px;
                text-align: center;
                background-color: transparent;
            }
            .navbar>.container .navbar-brand-centered,
            .navbar>.container-fluid .navbar-brand-centered {
                margin-left: -80px;
            }

        </style>


          <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@7.27.0/dist/sweetalert2.min.css">
        {{--<link href="{{asset('css/app.css')}}" rel="stylesheet" type="text/css">--}}


        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>


        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>


        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.18/datatables.min.css"/>

        <script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.18/datatables.min.js"></script>

    </head>
    <body>

    <nav class="navbar navbar-default" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-brand-centered">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navbar-brand navbar-brand-centered"><a  class="lead" href=" {{route('inicio')}}">CORPUS &ohm;&mho;</a> </div>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navbar-brand-centered">
                <ul class="nav navbar-nav">
                    <li><a href="{{route('tarea.bigrama')}}">Bigrama</a></li>
                    <li><a href="{{route('tarea.trigrama')}}">Trigrama</a></li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Operaciones <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{route('devolvertablahmmsuma')}}">Suma</a></li>
                            <li><a href="{{route('devolvertablahmmresta')}}">Resta</a></li>
                            <li><a href="{{route('devolvertablahmmmultipli')}}">Multiplicación</a></li>
                            <li><a href="{{route('devolvertablahmmdivisi')}}">División</a></li>

                            <li class="divider"></li>
                            <li><a href="{{route('devolvertablahmmvector')}}">Variables</a></li>
                            <li class="divider"></li>
                            <li><a href="{{route('devolvertablahmmbasura')}}">Basura</a></li>
                            <li><a href="{{route('devolvertablahmm')}}">Todo</a></li>
                        </ul>
                    </li>


                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Markov <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{route('basura')}}">Oraciones Basura</a></li>
                            <li><a href="{{route('tarea.markov')}}">Aparición de palabras</a></li>
                            <li><a href="{{route('tarea.mapacaracteres')}}">Mapa de Caracteres</a></li>
                            <li><a href="{{route('tarea.oracionnumeracion')}}">Listado Palabra</a></li>
                            {{--<li><a href="#">División</a></li>--}}

                            <li class="divider"></li>
                            <li><a href="{{route('operaciones.suma')}}">Suma</a></li>
                            <li><a href="{{route('operaciones.resta')}}">Resta</a></li>
                            <li><a href="{{route('operaciones.multplicacion')}}">Multiplicación</a></li>


                            <li><a href="{{route('operaciones.division')}}">Division</a></li>
                            <li><a href={{route('operaciones.variable')}}#">Variables</a></li>
                            <li class="divider"></li>
                            <li><a href="{{route('operaciones.basura')}}">Basura</a></li>
                        </ul>
                    </li>

                    {{--<li><a href="{{route('tarea.markov')}}">Markov</a></li>--}}
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">Nosotros</a></li>
                    <li><a href="#">Preguntas Frecuentes</a></li>
                    {{--<li class="dropdown">--}}
                        {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <span class="caret"></span></a>--}}
                        {{--<ul class="dropdown-menu" role="menu">--}}
                            {{--<li><a href="#">Action</a></li>--}}
                            {{--<li><a href="#">Another action</a></li>--}}
                            {{--<li><a href="#">Something else here</a></li>--}}
                            {{--<li class="divider"></li>--}}
                            {{--<li><a href="#">Separated link</a></li>--}}
                            {{--<li class="divider"></li>--}}
                            {{--<li><a href="#">One more separated link</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

<div  id="crud" >
    @yield('content')

</div>
<script >
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>


    <script >
        $(document).ready(function() {
            $('#example1').DataTable();
        } );
    </script>


    <script >
        $(document).ready(function() {
            $('#example2').DataTable();
        } );
    </script>


<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.27.0/dist/sweetalert2.all.min.js"></script>


    </body>
</html>
