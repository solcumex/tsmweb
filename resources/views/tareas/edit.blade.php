@extends('app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="col-md-7 col-md-offset-2">
            <form role="form"  method="POST"  action="{{route('tareaUpdate', $tarea->id_tarea) }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label for="titulo">Titulo </label>
                    <input type="text" class="form-control" value="{{$tarea->titulo}}" id="titulo" name="titulo" placeholder="Titulo">
                </div>


                <div class="form-group">
                    <label for="descripcion">Descripción</label>
                    <textarea class="form-control"  name="descripcion" id="descripcion" rows="3">
{{$tarea->descripcion}}

            </textarea>
                    {{--<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">--}}
                </div>


                <div class="form-group">
                    <label for="titulo">Prioridad </label>
                    <input type="text" class="form-control" value="{{$tarea->nivel_de_prioridad}}" id="nivel_de_prioridad" name="nivel_de_prioridad" placeholder="Titulo">
                </div>
                <input type="hidden" value="{{$tarea->id_tarea}}">
                {{--<div class="form-group">--}}
                {{--<label for="exampleInputFile">File input</label>--}}
                {{--<input type="file" id="exampleInputFile">--}}
                {{--<p class="help-block">Example block-level help text here.</p>--}}
                {{--</div>--}}
                {{--<div class="checkbox">--}}
                {{--<label>--}}
                {{--<input type="checkbox"> Check me out--}}
                {{--</label>--}}
                {{--</div>--}}
                <button type="submit" class="btn btn-primary">Guardar</button>
            </form>
            </div>
        </div>
    </div>


    @endsection