@extends('app')

@section('content')

    {{--<div class="container">--}}
        <div class="row">
<div class="container">
            <p></p>
            <h1>Corpus Matemático cuenta con  {{$information->cantpalabra}} palabras totales </h1>
            <p>EL Corpus matemático esta compuesto por {{$information->cantpalabrasdiferentes}} palabras diferentes y {{$information->cantoracionesdiferetes}} sentencias ademas esta la basura que son las cosas basura.</p>
            {{--<p>A simple example of how-to put a bordered table within a panel. Responsive, place holders in header/footer for buttons or pagination.</p>--}}
            {{--<p>Siguelo  <a href="https://twitter.com/asked_io" target="_new">@asked_io</a> & <a href="https://asked.io/" target="_new">asked.io</a>.</p>--}}
            <p> </p><p> </p>
</div>
            <div class="col-md-10 col-md-offset-1">

                <div class="panel panel-default panel-table">
                    <div class="panel-heading" style="margin-bottom: 1em">
                        <div class="row">
                            <div class="col col-xs-6">
                                <h3 class="panel-title">Corpus Matemático</h3>
                            </div>
                            <div class="col col-xs-6 text-right">
                                <a type="button" href="{{route('tarea.create')}}" class="btn btn-sm btn-primary btn-create">Crear</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table  id="example" class="table table-striped table-bordered table-list">
                            <thead>
                            <tr>
                                <th><em class="fa fa-cog"></em></th>
                                <th class="hidden-xs">ID</th>
                                <th> Titulo </th>
                                <th>Descripcion </th>
                                <th>Prioridad</th>
                                <th>Operación</th>
                                <th>Basura</th>


                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($task as $item)
                            <tr>
                                <td align="center">
                                    <a class="btn btn-default"    href="{{action('TareasController@edit', $item->id_tarea)}}"> <em class="fa fa-pencil"></em></a>


                                    <a class="btn btn-danger"  onclick="eliminarusuarioid({{$item->id_tarea}})"> <em class="fa fa-trash"></em></a>
                                </td>
                                <td class="hidden-xs">{{$item->id_tarea}}</td>
                                <td>{{$item->titulo}} 	</td>
                                <td>{{$item->descripcion }} 	</td>
                                <td>{{$item->nivel_de_prioridad}} 	</td>
                                <td>{{$item->operacion}} 	</td>
                                <td>{{$item->basura}} 	</td>

                            </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                    {{--<div class="panel-footer">--}}
                        {{--<div class="row">--}}
                            {{--<div class="col col-xs-4">Page 1 of 5--}}
                            {{--</div>--}}
                            {{--<div class="col col-xs-8">--}}
                                {{--<ul class="pagination hidden-xs pull-right">--}}
                                    {{--<li><a href="#">1</a></li>--}}
                                    {{--<li><a href="#">2</a></li>--}}
                                    {{--<li><a href="#">3</a></li>--}}
                                    {{--<li><a href="#">4</a></li>--}}
                                    {{--<li><a href="#">5</a></li>--}}
                                {{--</ul>--}}
                                {{--<ul class="pagination visible-xs pull-right">--}}
                                    {{--<li><a href="#">«</a></li>--}}
                                    {{--<li><a href="#">»</a></li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}


            </div></div>
    </div>

    {{--{{$task}}--}}


    <script>

        function eliminarusuarioid(usuario_id) {
            // alert(usuario_id);


            swal({
                title: "Esta Seguro?",
                text: "Una vez eliminado, no podrá recuperar la estructura!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {


                        // var responsable = usuario_id;

                        // var usuario_ide=usuario_id;
                        $.ajax({
                            url: "tarea/" + usuario_id,
                            {{--url: "{{route('estructura/'+ usuario_ide)}}",--}}
                            data: "&_token={{ csrf_token()}}",
                            dataType: "json",
                            method: "DELETE",
                            success: function (result) {
                                if (result['result'] == 'ok') {

                                    swal("Poof! ¡La estructura ha sido eliminada!", {
                                        icon: "success",
                                        timer: 2500,
                                    });
                                    setTimeout(location.reload.bind(location), 2500);
// if (swal.close()) {
//
//
// }
                                    // location.reload();

                                    // cachan.delay('10000');


                                    // setTimeout(location.reload(),);


                                }
                                else {

                                }
                            },
                            fail: function () {
                            },
                            beforeSend: function () {
                            }
                        });


                        // $.ajax({
                        //
                        //     type: 'GET',
                        //     data: { responsable: usuario_id },
                        //     url: '@Url.Action("CargarResponsa", "RC_Volantes")',
                        //     success: function (result) {
                        //
                        //         var s = '<option value="-1">ESCOGE LA ZONA</option>';
                        //         for (var i = 0; i < result.length; i++) {
                        //             s += '<option value="' + result[i].Id + '">' + result[i].Name + '</option>';
                        //             $('#OtroResponsable').html(s);
                        //         }
                        //
                        //
                        //     }
                        //
                        // });


                    } else {
                        swal("La Estructura no se eliminó");
                    }
                });

            // $("#ide2").attr("value", usuario_id);


        }
    </script>
    @endsection